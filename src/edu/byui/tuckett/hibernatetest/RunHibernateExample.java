/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU
 */
package edu.byui.tuckett.hibernatetest;

import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        //t.setCustomer("Trent", "Grantsville", "801-123-9710");
        Customer me = t.getCustomer(3);
        System.out.println(me);

        //t.updateCustomer(3, "Trent", "Grantsville", "801-123-9710");


        List<Customer> c = t.getCustomers();
        for (Customer i : c) {
            System.out.println(i);
        }
    }
}

